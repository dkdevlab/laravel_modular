<?php

namespace App\Modules\User\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    public function __invoke(Request $request): \Inertia\Response|\Inertia\ResponseFactory
    {
        return inertia('About', [
            'message' => 'Message from About component for User route'
        ]);
    }
}
