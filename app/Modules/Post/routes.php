<?php

use App\Modules\Post\Controllers\ {
    PostController
};

Route::prefix('post')
    ->as('post::')
    ->group(function () {
        Route::get('list', [PostController::class, 'index'])->name('list');
    });
